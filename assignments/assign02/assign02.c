// Must declare the main assembly entry point before use.
void main_asm();

/*
 * Main entry point for the code - simply calls the main assembly function.
 */
int main() {
    main_asm();
    return(0);
}

/* -------------------------------------------- Global Variables -------------------------------------------- */

int Remaining_Lives = 3; // Current lives the player has
int Gained_Lives = 0;    // Records the number of lives the player has gained
int Correct_Counter = 0; // records the number of correct answers in a row
int levelIndex = 0;
char *userInput;     // This array stores the user inputs
int userInputLength; // The length of the array userInput
int lastInput;

char inputSequence[20];
int i = 0; // i = length of input sequence
int tmpIndex = 0;
int inputComplete = 0;
int Remain_Counter = 5;
int Wrong_Counter = 0;

int select_level_input = 0;
/* -------------------------------------------- Morse Code Table -------------------------------------------- */

#define table_SIZE 36
typedef struct morsecode
{
    char character;
    char *code;
} morsecode;
morsecode table[table_SIZE];

void morse_init()
{

    table[0].character = 'A';table[1].character = 'B';table[2].character = 'C';table[3].character = 'D';table[4].character = 'E';table[5].character = 'F';
    table[6].character = 'G';table[7].character = 'H';table[8].character = 'I';table[9].character = 'J';table[10].character = 'K';
    table[11].character = 'L';table[12].character = 'M';table[13].character = 'N';table[14].character = 'O';table[15].character = 'P';
    table[16].character = 'Q';table[17].character = 'R';table[18].character = 'S';table[19].character = 'T';table[20].character = 'U';
    table[21].character = 'V';table[22].character = 'W';table[23].character = 'X';table[24].character = 'Y';table[25].character = 'Z';
    table[26].character = '0'; table[27].character = '1'; table[28].character = '2';table[29].character = '3';
    table[30].character = '4';table[31].character = '5';table[32].character = '6';table[33].character = '7';table[34].character = '8';table[35].character = '9';

    table[0].code = ".-";table[1].code = "-...";table[2].code = "-.-.";table[3].code = "-..";table[4].code = ".";table[5].code = "..-.";table[6].code = "--.";
    table[7].code = "....";table[8].code = "..";table[9].code = ".---";table[10].code = "-.-";table[11].code = ".-..";table[12].code = "--";table[13].code = "-.";
    table[14].code = "---";table[15].code = ".--.";table[16].code = "--.-";table[17].code = ".-.";table[18].code = "...";table[19].code = "-";table[20].code = "..-";
    table[21].code = "...-";table[22].code = ".--";table[23].code = "-..-";table[24].code = "-.--";table[25].code = "--..";table[26].code = "-----";table[27].code = ".----";
    table[28].code = "..---";table[29].code = "...--";table[30].code = "....-";table[31].code = ".....";table[32].code = "-....";
    table[33].code = "--...";table[34].code = "---..";table[35].code = "----.";
}

static const char *morseCodeMapping[] = {
    ".-",    // A
    "-...",  // B
    "-.-.",  // C
    "-..",   // D
    ".",     // E
    "..-.",  // F
    "--.",   // G
    "....",  // H
    "..",    // I
    ".---",  // J
    "-.-",   // K
    ".-..",  // L
    "--",    // M
    "-.",    // N
    "---",   // O
    ".--.",  // P
    "--.-",  // Q
    ".-.",   // R
    "...",   // S
    "-",     // T
    "..-",   // U
    "...-",  // V
    ".--",   // W
    "-..-",  // X
    "-.--",  // Y
    "--..",  // Z
    "-----", // 0
    ".----", // 1
    "..---", // 2
    "...--", // 3
    "....-", // 4
    ".....", // 5
    "-....", // 6
    "--...", // 7
    "---..", // 8
    "----.", // 9
};

static const char *alphaNumericList[] = {
"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","0","1","2","3","4","5","6","7","8","9",
};
/* -------------------------------------------- Level Selection -------------------------------------------- */

void level_select_true(){
    select_level_input = 1;
}

void level_select_false(){
    select_level_input = 0;
}

int select_level(){

    if (strcmp(inputSequence, morseCodeMapping[27])==0){    // return 1 for level 1
        printf("Level 1 selected");
        return 1;
    }
    else if(strcmp(inputSequence, morseCodeMapping[28])==0){    // return 2 for level 2
        printf("Level 2 selected");
        return 2;
    }
}
// Random number generator, for getting a random letter/number from the morese code table
int random(int low, int high)
{
    if (low > high)
        return high;
    return low + (rand() % (high - low + 1));
}
int level_1_question()
{
    levelIndex = 1;
    int tmp = random(0, 35);
    tmpIndex = tmp;
    printf("\n");
    printf("____________________________________________________\n");
    printf("[]________________________________________________[]\n");
    printf("[]                                                []\n");
    printf("[]     Translate this character to Morse code     []\n");
    printf("[]                       %s                       []\n",morseCodeMapping[tmp]);
    printf("[]                       %s                       []\n",alphaNumericList[tmp]);
    printf("[]                                                []\n");
    printf("[]________________________________________________[]\n");
    printf("[]________________________________________________[]\n");
    return tmp;
}

int level_2_question()
{
    levelIndex = 2;
    int tmp = random(0, 35);
    tmpIndex = tmp;
    printf("\n");
    printf("____________________________________________________\n");
    printf("[]________________________________________________[]\n");
    printf("[]                                                []\n");
    printf("[]                                                []\n");
    printf("[]           What is this in morse code?          []\n");
    printf("[]                       %c                       []\n",table[tmp].character);
    printf("[]                                                []\n");
    printf("[]________________________________________________[]\n");
    printf("[]________________________________________________[]\n");
    return tmp;
}
int get_level()
{
    return levelIndex;
}

void set_level(int level)
{
    levelIndex = level;
}
int check_level_complete()
{
    if (Correct_Counter == 5)
    {
        return 1;
    }
    if (Remaining_Lives == 0)
    {
        printf("\n");
        printf("____________________________________________________\n");
        printf("[]________________________________________________[]\n");
        printf("[]                                                []\n");
        printf("[]                                                []\n");
        printf("[]               GAME OVER; You lose              []\n");
        printf("[]           You have no lives remaining          []\n");
        printf("[]                                                []\n");
        printf("[]________________________________________________[]\n");
        printf("[]________________________________________________[]\n");
        
        printf("\n");
        printf("____________________________________________________\n");
        printf("[]________________________________________________[]\n");
        printf("[]               ~ ~ Stats Table ~ ~              []\n");
        printf("[]                                                []\n");
        printf("[]               Correct answers: %d              []\n", Correct_Counter);
        printf("[]              Incorrect answers: %d             []\n", Wrong_Counter);
        printf("[]               Lives collected: %d              []\n", Gained_Lives);
        printf("[]               Remianing Lives: %d              []\n", Remaining_Lives);
        printf("[]________________________________________________[]\n");
        printf("[]________________________________________________[]\n");
        printf("\n");
        return 2;
    }
return 0;
}
